const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

const actionTypes = [
  'navigation',
  'crud',
  'various',
];

const navigationActions = [
  'open_app',
  'go_to',
  'close_app',
];

const crudActions = [
  'create',
  'read',
  'update',
  'delete',
];

const variousActions = [
  'get_estimation',
];

const allActions = [].concat(navigationActions, crudActions, variousActions);

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const userActions = sequelizeClient.define('user_actions', {
    userId: {
      type: DataTypes.UUID,
      field: 'user_id',
    },
    client: {
      type: DataTypes.ENUM('web', 'mobile'),
      allowNull: false,
    },
    action: {
      type: DataTypes.ENUM(...allActions),
      allowNull: false,
    },
    type: {
      type: DataTypes.ENUM(...actionTypes),
      allowNull: false,
    },
    payload: {
      type: DataTypes.JSONB,
    },
  }, {
    timestamps: true,
    createdAt: 'time',
    updatedAt: false,
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  userActions.removeAttribute('id');

  userActions.associate = (models) => { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return userActions;
};
