const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const payments = sequelizeClient.define('payments', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    externalId: {
      type: DataTypes.UUID,
      field: 'external_id',
    },
    monthsNumber: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 36,
      },
      field: 'months_number',
    },
    url: {
      type: DataTypes.STRING,
      validate: {
        len: [1, 256],
      },
    },
    isPaid: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
      field: 'is_paid',
    },
    paidTill: {
      type: DataTypes.DATE,
      field: 'paid_till',
    },
    receptionTime: {
      type: DataTypes.DATE,
      field: 'reception_time',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'updated_at',
    },
  }, {
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  payments.associate = (models) => {
    models.payments.belongsTo(models.companies, {
      foreignKey: {
        name: 'companyId',
        field: 'company_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
    models.payments.belongsTo(models.users, {
      foreignKey: {
        name: 'userId',
        field: 'user_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT',
    });
    models.payments.belongsTo(models.tariff_costs, {
      foreignKey: {
        name: 'tariffCostId',
        field: 'tariff_cost_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'RESTRICT',
    });
  };

  return payments;
};
