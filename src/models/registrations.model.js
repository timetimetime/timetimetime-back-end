const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const registrations = sequelizeClient.define('registrations', {
    ip: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        isIP: true,
        len: [7, 45],
      },
    },
  }, {
    timestamps: true,
    createdAt: 'time',
    updatedAt: false,
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  registrations.removeAttribute('id');

  // eslint-disable-next-line no-unused-vars
  registrations.associate = (models) => {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return registrations;
};
