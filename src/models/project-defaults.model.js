const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const projectDefaults = sequelizeClient.define('project_defaults', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    salesmanCommission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 100,
      },
      field: 'salesman_commission',
    },
    salesmanCommissionBase: {
      type: DataTypes.ENUM('total', 'income'),
      defaultValue: 'total',
      allowNull: false,
      field: 'salesman_commission_base',
    },
    taxes: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 100,
      },
    },
    insurance: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 1024,
      },
    },
    income: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 1024,
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'updated_at',
    },
  }, {
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  projectDefaults.associate = (models) => {
    models.project_defaults.belongsTo(models.companies, {
      foreignKey: {
        name: 'companyId',
        field: 'company_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
  };

  return projectDefaults;
};
