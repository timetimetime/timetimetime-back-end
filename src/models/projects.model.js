const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const projects = sequelizeClient.define('projects', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [1, 64],
      },
    },
    salesmanCommission: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 100,
      },
      field: 'salesman_commission',
    },
    salesmanCommissionBase: {
      type: DataTypes.ENUM('total', 'income'),
      defaultValue: 'total',
      allowNull: false,
      field: 'salesman_commission_base',
    },
    taxes: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 100,
      },
    },
    insurance: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 1024,
      },
    },
    income: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        min: 0,
        max: 1024,
      },
    },
    type: {
      type: DataTypes.ENUM('waterfall', 'agile'),
      allowNull: false,
    },
    uri: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: 'uniqueProjectName',
      validate: {
        len: [1, 128],
      },
    },
    disabledSubtaskCategories: {
      type: DataTypes.ARRAY(Sequelize.UUID),
      allowNull: false,
      defaultValue: [],
      field: 'disabled_subtask_categories',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'updated_at',
    },
  }, {
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  projects.associate = (models) => { // eslint-disable-line no-unused-vars
    const { projects, tasks, services } = models;
    projects.hasMany(tasks, {
      foreignKey: {
        name: 'projectId',
        field: 'project_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
    projects.belongsTo(services, {
      foreignKey: {
        name: 'serviceId',
        field: 'service_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
  };

  return projects;
};
