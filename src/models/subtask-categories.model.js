const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

module.exports = (app) => {
  const sequelizeClient = app.get('sequelizeClient');
  const subtaskCategories = sequelizeClient.define('subtask_categories', {
    id: {
      primaryKey: true,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [1, 64],
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
      field: 'updated_at',
    },
  }, {
    underscored: true,
    hooks: {
      beforeCount(options) {
        options.raw = true;
      },
    },
  });

  subtaskCategories.associate = (models) => {
    const { companies, subtask_categories, services } = models;
    subtask_categories.belongsToMany(subtask_categories, {
      as: 'predecessors',
      through: models.subtask_categories_order,
    });
    subtask_categories.belongsTo(services, {
      foreignKey: {
        name: 'serviceId',
        field: 'service_id',
        allowNull: false,
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    });
    subtask_categories.belongsToMany(models.performers, {
      through: models.performer_positions,
    });
  };

  return subtaskCategories;
};
