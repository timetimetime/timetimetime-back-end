const postmark = require('postmark');

const { POSTMARK_API_TOKEN } = process.env;

const client = new postmark.Client(POSTMARK_API_TOKEN);

const defaults = {
  From: 'TimeTimeTime <time@timetimeti.me>',
};

module.exports = {
  send(data) {
    const options = Object.assign({}, defaults, data);
    client.sendEmail(options);
  },
};
