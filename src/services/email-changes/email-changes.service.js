// Initializes the `EmailChanges` service on path `/email-changes`
const createService = require('feathers-sequelize');
const createModel = require('../../models/email-changes.model');
const hooks = require('./email-changes.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'email-changes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/email-changes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('email-changes');

  service.hooks(hooks);
};
