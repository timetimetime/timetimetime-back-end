const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  discard,
  disallow,
  iff,
  isProvider,
} = require('feathers-hooks-common');
const onlyOwnUser = require('../../hooks/only-own-user');
const handleEmailChanging = require('../../hooks/handle-email-changing');
const handleNewUserEmailConfirmation = require('../../hooks/handle-new-user-email-confirmation');
const handlePasswordChanging = require('../../hooks/handle-password-changing');
const checkRegistrationLimit = require('../../hooks/check-registration-limit');
const createRegistrationRecord = require('../../hooks/create-registration-record');

const {
  hashPassword,
  protect,
} = require('@feathersjs/authentication-local').hooks;

const hideSecretFields = iff(
  isProvider('external'),
  discard(
    'emailConfirmationCode',
    'unsubscriptionCode',
  ),
);

const registrationLimit = iff(
  isProvider('external'),
  checkRegistrationLimit(),
);

module.exports = {
  before: {
    all: [],
    find: [disallow('external')],
    get: [authenticate('jwt'), onlyOwnUser()],
    create: [registrationLimit, hashPassword()],
    update: [disallow()],
    patch: [authenticate('jwt'), onlyOwnUser(), handlePasswordChanging(), hashPassword(), handleEmailChanging()],
    remove: [authenticate('jwt'), onlyOwnUser()],
  },

  after: {
    all: [
      protect('password'),
    ],
    find: [hideSecretFields],
    get: [hideSecretFields],
    create: [handleNewUserEmailConfirmation(), createRegistrationRecord(), hideSecretFields],
    update: [hideSecretFields],
    patch: [hideSecretFields],
    remove: [hideSecretFields],
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
