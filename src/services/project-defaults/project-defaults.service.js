// Initializes the `project-defaults` service on path `/project-defaults`
const createService = require('feathers-sequelize');
const createModel = require('../../models/project-defaults.model');
const hooks = require('./project-defaults.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'project-defaults',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/project-defaults', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('project-defaults');

  service.hooks(hooks);
};
