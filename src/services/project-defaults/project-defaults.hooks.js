const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany()],
    get: [disallow('external')],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [actionOnAvailableCompany()],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
