const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const populateCompany = require('../../hooks/populate-role-company');
const rawFalse = require('../../hooks/raw-false');
const onlyOwnRoles = require('../../hooks/only-own-roles');

module.exports = {
  before: {
    all: [],
    find: [authenticate('jwt'), onlyOwnRoles(), rawFalse(), populateCompany()],
    get: [disallow('external')],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
