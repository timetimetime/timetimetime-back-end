const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');
const checkProjectsNumber = require('../../hooks/check-projects-number');
const setProjectDefaults = require('../../hooks/set-project-defaults');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany()],
    get: [actionOnAvailableCompany()],
    create: [actionOnAvailableCompany(), checkProjectsNumber, setProjectDefaults()],
    update: [disallow('external')],
    patch: [actionOnAvailableCompany()],
    remove: [actionOnAvailableCompany()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
