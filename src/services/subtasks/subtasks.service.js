// Initializes the `subtasks` service on path `/subtasks`
const createService = require('feathers-sequelize');
const createModel = require('../../models/subtasks.model');
const hooks = require('./subtasks.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'subtasks',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/subtasks', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('subtasks');

  service.hooks(hooks);
};
