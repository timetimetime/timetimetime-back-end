const {
  disallow,
  discard,
  isProvider,
  iff,
} = require('feathers-hooks-common');
const sendConfirmationLetter = require('../../hooks/send-password-recovery-confirmation-letter');
const passwordCanBeRecovered = require('../../hooks/user-password-can-be-recovered');

const hideSecretFields = iff(
  isProvider('external'),
  discard(
    'confirmationCode',
    'isConfirmed',
  ),
);

module.exports = {
  before: {
    all: [],
    find: [disallow('external')],
    get: [disallow('external')],
    create: [passwordCanBeRecovered()],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [sendConfirmationLetter(), hideSecretFields],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
