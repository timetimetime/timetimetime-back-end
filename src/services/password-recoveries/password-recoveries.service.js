// Initializes the `PasswordRecoveries` service on path `/password-recoveries`
const createService = require('feathers-sequelize');
const createModel = require('../../models/password-recoveries.model');
const hooks = require('./password-recoveries.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'password-recoveries',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/password-recoveries', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('password-recoveries');

  service.hooks(hooks);
};
