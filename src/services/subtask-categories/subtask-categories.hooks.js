const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const dehydrate = require('feathers-sequelize/hooks/dehydrate');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');
const rawFalse = require('../../hooks/raw-false');
const setSubtaskCategoryDescendants = require('../../hooks/set-subtask-category-descendants');
const populatePredecessors = require('../../hooks/populate-subtask-category-predecessors');
const cleanupDisabledSubtaskCategories = require('../../hooks/cleanup-disabled-subtask-categories');
const checkModelsNumberLimit = require('../../hooks/check-models-number-limit');
const modelsToIds = require('../../hooks/models-to-ids');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany(), rawFalse(), populatePredecessors()],
    get: [actionOnAvailableCompany()],
    create: [actionOnAvailableCompany(), checkModelsNumberLimit(), rawFalse()],
    update: [disallow('external')],
    patch: [actionOnAvailableCompany(), rawFalse()],
    remove: [actionOnAvailableCompany()],
  },

  after: {
    all: [],
    find: [dehydrate(), modelsToIds('predecessors')],
    get: [],
    create: [setSubtaskCategoryDescendants()],
    update: [],
    patch: [setSubtaskCategoryDescendants()],
    remove: [cleanupDisabledSubtaskCategories()],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
