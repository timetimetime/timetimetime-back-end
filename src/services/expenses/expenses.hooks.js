const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');
const checkModelsNumberLimit = require('../../hooks/check-models-number-limit');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany()],
    get: [actionOnAvailableCompany()],
    create: [actionOnAvailableCompany(), checkModelsNumberLimit()],
    update: [disallow('external')],
    patch: [actionOnAvailableCompany()],
    remove: [actionOnAvailableCompany()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
