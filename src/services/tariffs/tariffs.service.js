// Initializes the `Tariffs` service on path `/tariffs`
const createService = require('feathers-sequelize');
const createModel = require('../../models/tariffs.model');
const hooks = require('./tariffs.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'tariffs',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/tariffs', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('tariffs');

  service.hooks(hooks);
};
