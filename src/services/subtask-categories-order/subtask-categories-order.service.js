// Initializes the `SubtaskCategoriesOrder` service on path `/subtask-categories-order`
const createService = require('feathers-sequelize');
const createModel = require('../../models/subtask-categories-order.model');
const hooks = require('./subtask-categories-order.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'subtask-categories-order',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/subtask-categories-order', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('subtask-categories-order');

  service.hooks(hooks);
};
