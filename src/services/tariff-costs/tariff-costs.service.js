// Initializes the `TariffCosts` service on path `/tariff-costs`
const createService = require('feathers-sequelize');
const createModel = require('../../models/tariff-costs.model');
const hooks = require('./tariff-costs.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'tariff-costs',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/tariff-costs', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('tariff-costs');

  service.hooks(hooks);
};
