// Initializes the `PerformerPositions` service on path `/performer-positions`
const createService = require('feathers-sequelize');
const createModel = require('../../models/performer-positions.model');
const hooks = require('./performer-positions.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'performer-positions',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/performer-positions', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('performer-positions');

  service.hooks(hooks);
};
