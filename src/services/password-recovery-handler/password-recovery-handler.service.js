// Initializes the `passwordRecoveryHandler` service on path `/password-recovery-handler`
const createService = require('./password-recovery-handler.class.js');
const hooks = require('./password-recovery-handler.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    name: 'password-recovery-handler',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/password-recovery-handler', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('password-recovery-handler');

  service.hooks(hooks);
};
