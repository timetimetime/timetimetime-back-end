const errors = require('@feathersjs/errors');
const moment = require('moment');

/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  // async find (params) {
  //   return [];
  // }

  // async get (id, params) {
  //   return {
  //     id, text: `A new message with ID: ${id}!`
  //   };
  // }

  async create(data, params) {
    const { password, confirmationCode } = data;
    const passwordRecoveries = await this.app.service('password-recoveries').patch(null, {
      isConfirmed: true,
    }, {
      query: {
        confirmationCode,
        isConfirmed: false,
        createdAt: {
          $gt: moment().subtract(12, 'hours').format(),
        },
        $limit: 1,
      },
    });
    if (!passwordRecoveries || passwordRecoveries.length === 0) {
      throw new errors.BadRequest('NO_PASSWORD_RECOVERY');
    }
    const passwordRecovery = passwordRecoveries[0];
    const result = await this.app.service('users').patch(null, {
      password,
    }, {
      query: {
        id: passwordRecovery.userId,
      },
    });
    return {
      status: result.length > 0 ? 'success' : 'failure',
    };
  }

  setup(app) {
    this.app = app;
  }

  // async update (id, data, params) {
  //   return data;
  // }

  // async patch (id, data, params) {
  //   return data;
  // }

  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = (options) => {
  return new Service(options);
};

module.exports.Service = Service;
