const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const createDefaultRelatedModels = require('../../hooks/create-default-company-related-models');
const onlyOwnCompanies = require('../../hooks/only-own-companies');
const checkOwnCompaniesLimit = require('../../hooks/check-own-companies-limit');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [onlyOwnCompanies()],
    get: [disallow('external')],
    create: [checkOwnCompaniesLimit()],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [createDefaultRelatedModels()],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
