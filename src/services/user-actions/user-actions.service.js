// Initializes the `appUsageLogs` service on path `/app-usage-logs`
const createService = require('feathers-sequelize');
const createModel = require('../../models/user-actions.model');
const hooks = require('./user-actions.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'user-actions',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/talia-winters', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('talia-winters');

  service.hooks(hooks);
};
