// {
//   "type": "notification",
//   "event": "payment.waiting_for_capture",
//   "object": {
//     "id": "21740069-000f-50be-b000-0486ffbf45b0",
//     "status": "waiting_for_capture",
//     "paid": true,
//     "amount": {
//       "value": "2.00",
//       "currency": "RUB"
//     },
//     "created_at": "2017-10-14T10:53:29.072Z",
//     "metadata": {},
//     "payment_method": {
//       "type": "yandex_money",
//       "id": "731714f2-c6eb-4ae0-aeb6-8162e89c1065",
//       "saved": false,
//       "account_number": "410011066000000",
//       "title": "Yandex.Money wallet 410011066000000"
//     }
//   }
// }

const axios = require('axios');
const moment = require('moment');

class PaymentHandler {
  setup(app) {
    this.app = app;
  }
  async create(data) {
    if (data.type === 'notification' && data.event === 'payment.succeeded') {
      const { shopId, shopSecret } = this.app.get('paymentSystem');
      const paymentId = data.object.id;
      const response = await axios.get(`https://payment.yandex.net/api/v3/payments/${paymentId}`, {
        auth: {
          username: shopId,
          password: shopSecret,
        },
      });
      const payment = response.data;
      if (payment.paid === true && payment.status === 'succeeded') {
        const previousPayments = await this.app.service('payments').find({
          query: {
            $limit: 1,
            $sort: {
              createdAt: -1,
            },
            companyId: payment.metadata.companyId,
            isPaid: true,
          },
        });
        const previousPayment = previousPayments.data[0];
        const paidSubscriptionDays = payment.metadata.monthsNumber * 31;
        let shouldBePaidTill;
        if (previousPayment && moment(previousPayment.paidTill).isAfter(moment())) {
          shouldBePaidTill = moment(previousPayment.paidTill).add(paidSubscriptionDays, 'days').format();
        } else {
          shouldBePaidTill = moment().add(paidSubscriptionDays, 'days').format();
        }
        const updatedPayment = await this.app.service('payments').patch(payment.metadata.paymentId, {
          isPaid: true,
          paidTill: shouldBePaidTill,
          receptionTime: moment().format(),
        });
        return updatedPayment;
      }
    }
    return data;
  }
}

module.exports = () => new PaymentHandler();

module.exports.Service = PaymentHandler;
