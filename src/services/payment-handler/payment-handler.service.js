// Initializes the `PaymentHandler` service on path `/payment-handler`
const createService = require('./payment-handler.class.js');
const hooks = require('./payment-handler.hooks');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    name: 'payment-handler',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/payment-handler', createService(options), (req, res, next) => {
    if (req.method === 'POST') {
      res.statusCode = 200;
    }
    next();
  });

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('payment-handler');

  service.hooks(hooks);
};
