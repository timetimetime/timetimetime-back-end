const { authenticate } = require('@feathersjs/authentication').hooks;
const { disallow } = require('feathers-hooks-common');
const dehydrate = require('feathers-sequelize/hooks/dehydrate');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');
const checkModelsNumberLimit = require('../../hooks/check-models-number-limit');
const setPerformerPositions = require('../../hooks/set-performer-positions');
const rawFalse = require('../../hooks/raw-false');
const populatePerformersPositions = require('../../hooks/populate-performer-positions');
const modelsToIds = require('../../hooks/models-to-ids');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany(), rawFalse(), populatePerformersPositions()],
    get: [actionOnAvailableCompany()],
    create: [actionOnAvailableCompany(), checkModelsNumberLimit(), rawFalse()],
    update: [disallow('external')],
    patch: [actionOnAvailableCompany(), rawFalse()],
    remove: [actionOnAvailableCompany()],
  },

  after: {
    all: [],
    find: [dehydrate(), modelsToIds('subtaskCategories')],
    get: [],
    create: [setPerformerPositions()],
    update: [],
    patch: [setPerformerPositions()],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
