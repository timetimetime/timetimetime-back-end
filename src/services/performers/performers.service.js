// Initializes the `Performers` service on path `/performers`
const createService = require('feathers-sequelize');
const createModel = require('../../models/performers.model');
const hooks = require('./performers.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'performers',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/performers', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('performers');

  service.hooks(hooks);
};
