// Initializes the `EmailSubscription` service on path `/email-subscription`
const createService = require('./email-handler.class.js');
const hooks = require('./email-handler.hooks');

module.exports = function (app) {

  const paginate = app.get('paginate');

  const options = {
    name: 'email-handler',
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/email-handler', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('email-handler');

  service.hooks(hooks);
};
