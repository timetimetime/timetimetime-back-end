const errors = require('@feathersjs/errors');
const moment = require('moment');

/* eslint-disable no-unused-vars */
class Service {
  constructor(options) {
    this.options = options || {};
  }

  // async find (params) {
  //   return [];
  // }

  // async get (id, params) {
  //   return {
  //     id, text: `A new message with ID: ${id}!`
  //   };
  // }

  async create(data, params) {
    const { code } = data;
    const emailChanges = await this.app.service('email-changes').patch(null, {
      isConfirmed: true,
    }, {
      query: {
        confirmationCode: code,
        isConfirmed: false,
        createdAt: {
          $gt: moment().subtract(3, 'days').format(),
        },
        $limit: 1,
      },
    });
    if (!emailChanges || emailChanges.length === 0) {
      throw new errors.BadRequest();
    }
    const emailChange = emailChanges[0];
    const result = await this.app.service('users').patch(emailChange.userId, {
      isEmailConfirmed: true,
      email: emailChange.email,
    });
    return {
      status: result.length > 0 ? 'success' : 'failure',
    };
  }

  setup(app) {
    this.app = app;
  }

  // async update (id, data, params) {
  //   return data;
  // }

  // async patch (id, data, params) {
  //   return data;
  // }

  // async remove (id, params) {
  //   return { id };
  // }
}

module.exports = (options) => {
  return new Service(options);
};

module.exports.Service = Service;
