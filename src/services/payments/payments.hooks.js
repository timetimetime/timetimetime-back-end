const { authenticate } = require('@feathersjs/authentication').hooks;
const {
  disallow,
  discard,
  iff,
  isProvider,
} = require('feathers-hooks-common');
const actionOnAvailableCompany = require('../../hooks/action-on-available-company');
const notifyPaymentSystem = require('../../hooks/notify-payment-system');
const setUserId = require('../../hooks/set-user-id');

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [actionOnAvailableCompany()],
    get: [actionOnAvailableCompany()],
    create: [actionOnAvailableCompany(), setUserId(), iff(isProvider('external'), discard('isPaid', 'receptionTime', 'paidTill', 'url', 'externalId'))],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [notifyPaymentSystem()],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
