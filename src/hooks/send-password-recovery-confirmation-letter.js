const mailer = require('../utils/mailer');

module.exports = () => {
  return async (context) => {
    const passwordRecovery = context.result;
    const host = context.app.get('host');
    const url = `${host}/set-new-password?code=${passwordRecovery.confirmationCode}`;
    mailer.send({
      To: context.data.email,
      Subject: 'Password recovery',
      HtmlBody: `<p>Please flow the <a href="${url}">link</a> to set a new password.</p>`,
    }, (error) => {
      if (error) {
        console.error("Unable to send via postmark: " + error.message);
      }
    });
    return context;
  };
};
