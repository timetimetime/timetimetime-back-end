const errors = require('@feathersjs/errors');

const limit = 16;

module.exports = () => {
  return async (context) => {
    const sequelizeClient = context.app.get('sequelizeClient');
    const sql = 'SELECT COUNT(*) AS count FROM roles WHERE user_id = :userId';
    const query = await sequelizeClient.query(sql, {
      replacements: {
        userId: context.params.payload.userId,
      },
    });
    const rows = query[0];
    const { count } = rows[0];
    if (count >= limit) {
      throw new errors.Forbidden('MODELS_NUMBER_LIMIT');
    }
    return context;
  };
};
