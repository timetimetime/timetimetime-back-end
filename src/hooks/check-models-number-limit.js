const errors = require('@feathersjs/errors');
const _ = require('lodash');

// TODO: companies

const limits = [
  {
    service: 'expenses',
    number: 256,
    via: 'companyId',
  },
  {
    service: 'performers',
    number: 256,
    via: 'companyId',
  },
  {
    service: 'services',
    number: 32,
    via: 'companyId',
  },
  {
    service: 'subtask-categories',
    number: 256,
    via: 'companyId',
  },
  {
    service: 'tasks',
    number: 256,
    via: 'projectId',
  },
  {
    service: 'subtasks',
    number: 64,
    via: 'taskId',
  },
];

const serviceNameToTableName = name => name.replace('-', '_');

module.exports = () => {
  return async (context) => {
    const sequelizeClient = context.app.get('sequelizeClient');
    const serviceName = context.path;
    const limit = limits.filter(el => el.service === serviceName)[0];
    const tableName = serviceNameToTableName(limit.service);
    const sql = `SELECT COUNT(*) AS count FROM ${tableName} WHERE "${_.snakeCase(limit.via)}" = :value`;
    const query = await sequelizeClient.query(sql, {
      replacements: {
        value: context.data[limit.via],
      },
    });
    const rows = query[0];
    const { count } = rows[0];
    if (count >= limit.number) {
      throw new errors.Forbidden('MODELS_NUMBER_LIMIT');
    }
    return context;
  };
};
