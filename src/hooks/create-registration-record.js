module.exports = () => {
  return async (context) => {
    context.app.service('registrations').create({
      ip: context.params.headers['x-real-ip'],
    });
    return context;
  };
};
