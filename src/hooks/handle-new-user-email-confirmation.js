module.exports = () => {
  return async (context) => {
    const user = context.result;
    if (process.env.NODE_ENV === 'production') {
      await context.app.service('email-changes').create({
        userId: user.id,
        email: user.email,
      });
    }
    return context;
  };
};
