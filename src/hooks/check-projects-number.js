const errors = require('@feathersjs/errors');
const moment = require('moment');

const limit = 32;

module.exports = async (context) => {
  const sequelizeClient = context.app.get('sequelizeClient');
  const { companyId } = context.data;
  // Get projects count for the company
  const sql = 'SELECT COUNT(*) AS count FROM projects WHERE company_id = :companyId';
  const query = await sequelizeClient.query(sql, {
    replacements: {
      companyId,
    },
  });
  const rows = query[0];
  const { count } = rows[0];
  // Get project number limit for the company
  const limitSql = `
    SELECT * FROM payments
    INNER JOIN tariff_costs ON payments.tariff_cost_id = tariff_costs.id
    INNER JOIN tariffs ON tariff_costs.tariff_id = tariffs.id
    WHERE
      company_id = :companyId
      AND is_paid = true
      AND paid_till > :now
      AND reception_time < :now
    LIMIT 1;
  `;
  const paymentQuery = await sequelizeClient.query(limitSql, {
    replacements: {
      companyId,
      now: moment().format(),
    },
  });
  const payment = paymentQuery[0][0];
  if (!payment) {
    throw new errors.Forbidden('PAYMENT_REQUIRED');
  }
  const { number_of_projects: maxProjectsNumber } = payment;
  // Check if the limit are not exceed
  if (count >= maxProjectsNumber) {
    throw new errors.Forbidden('MAX_PROJECT_NUMBER');
  }
  return context;
};
