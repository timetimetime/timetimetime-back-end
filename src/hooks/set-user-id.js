module.exports = () => {
  return (context) => {
    const { userId } = context.params.payload;
    context.data.userId = userId;
    return context;
  };
};
