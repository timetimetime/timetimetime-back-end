module.exports = () => {
  return async (context) => {
    if (context.params.provider && context.data.email) {
      await context.app.service('email-changes').create({
        userId: context.data.id,
        email: context.data.email,
      });
      delete context.data.email;
    }
    return context;
  };
};
