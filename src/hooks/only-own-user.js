const errors = require('@feathersjs/errors');

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return context => {
    if (context.params.provider) {
      if (!context.params.payload || context.id !== context.params.payload.userId) {
        throw new errors.Forbidden('Cannot get other users');
      }
    }
    return context;
  };
};
