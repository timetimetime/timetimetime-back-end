module.exports = () => {
  return async (context) => {
    const { companyId } = context.data;
    const subtaskCategory = context.result;
    const predecessors = context.data.predecessors || [];
    if (context.method === 'patch') {
      const patchedCategoryId = context.id;
      const sourceRecords = predecessors;
      const filteredRecords = sourceRecords.filter(value => value !== patchedCategoryId);
      await subtaskCategory.setPredecessors(filteredRecords, { through: { companyId } });
    } else if (context.method === 'create' && predecessors) {
      await subtaskCategory.setPredecessors(predecessors, { through: { companyId } });
    }
    context.result.dataValues.predecessors = predecessors;
    return context;
  };
};
