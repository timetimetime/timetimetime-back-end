module.exports = (field) => {
  return (context) => {
    context.result.data.forEach((el) => {
      el[field] = el[field].map(s => s.id);
    });
    return context;
  };
};
