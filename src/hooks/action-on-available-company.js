const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    if (!context.params.provider) {
      return context;
    }
    let companyId;
    if (context.method === 'find') {
      companyId = context.params.query && context.params.query.companyId;
    } else if (context.method === 'create') {
      companyId = context.data.companyId;
    } else if (context.method === 'patch' || context.method === 'remove' || context.method === 'get') {
      const recordId = context.id;
      const record = await context.service.get(recordId);
      if (!record) {
        const msg = 'Record not found';
        throw new errors.Forbidden(msg);
      }
      companyId = record.companyId;
    }
    const { app } = context;
    const userId = context.params.payload && context.params.payload.userId;
    if (!companyId || !userId) {
      const msg = `Required fields are not provided: companyId=${companyId} userId=${userId}`;
      throw new errors.Forbidden(msg);
    }
    try {
      const companies = await app.service('companies').find({
        query: { id: companyId },
        sequelize: {
          include: [
            { model: context.app.get('sequelizeClient').models.roles },
          ],
          raw: false,
        },
      });
      const company = companies.data[0];
      if (!company) {
        const msg = 'Company not found';
        throw new errors.Forbidden(msg);
      }
      const companyMembers = company.dataValues.roles.map(el => el.dataValues.userId);
      if (!companyMembers.includes(userId)) {
        const msg = 'No access to company';
        throw new errors.Forbidden(msg);
      }
    } catch (err) {
      throw err;
    }
    return context;
  };
};
