const errors = require('@feathersjs/errors');
const moment = require('moment');

const limit = {
  number: 2,
  minutes: 60,
};

module.exports = () => {
  return async (context) => {
    const sequelizeClient = context.app.get('sequelizeClient');
    const ip = context.params.headers['x-real-ip'];
    const from = moment().subtract(limit.minutes, 'minutes').format();
    const sql = 'SELECT COUNT(*) AS count FROM registrations WHERE ip = :ip AND "time" >= :from';
    const query = await sequelizeClient.query(sql, {
      replacements: { ip, from },
    });
    const rows = query[0];
    const { count } = rows[0];
    if (count >= limit.number && process.env.NODE_ENV === 'production') {
      throw new errors.Forbidden('TOO_MANY_REGISTRATIONS_PER_IP');
    }
    return context;
  };
};
