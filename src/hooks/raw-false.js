module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return context => {
    if (!context.params.sequelize) context.params.sequelize = {};
    Object.assign(context.params.sequelize, { raw: false });
    return context;
  };
};
