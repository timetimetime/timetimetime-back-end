const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    const { query, payload } = context.params;
    const userIdFilter = query && query.userId;
    const userId = payload && payload.userId;
    if (!userIdFilter || !userId || userIdFilter != userId) {
      throw new errors.Forbidden();
    }
    return context;
  };
};
