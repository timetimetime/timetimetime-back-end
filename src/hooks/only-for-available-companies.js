const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    if (context.params.provider) {
      const { app } = context;
      const { query, payload } = context.params;
      const companyId = query && query.companyId;
      const userId = payload && payload.userId;
      if (!companyId || !userId) {
        throw new errors.Forbidden();
      }
      try {
        const companies = await app.service('companies').find({
          query: { id: companyId },
          sequelize: {
            include: [
              { model: context.app.get('sequelizeClient').models.roles },
            ],
            raw: false,
          },
        });
        const company = companies.data[0];
        if (!company) {
          throw new errors.Forbidden();
        }
        const companyMembers = company.dataValues.roles.map(el => el.dataValues.userId);
        if (!companyMembers.includes(userId)) {
          throw new errors.Forbidden();
        }
      } catch (err) {
        throw err;
      }
    }
    return context;
  };
};
