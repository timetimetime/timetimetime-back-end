module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const sequelizeClient = context.app.get('sequelizeClient');
    context.params.sequelize.include = [
      {
        model: sequelizeClient.models.companies,
      },
    ];
    return context;
  };
};
