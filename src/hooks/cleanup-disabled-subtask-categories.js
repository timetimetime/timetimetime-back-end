module.exports = () => {
  return async (context) => {
    const sequelizeClient = context.app.get('sequelizeClient');
    const subtaskCategoryId = context.id;
    const { companyId, serviceId } = context.result;
    const sql = 'UPDATE projects SET disabled_subtask_categories = array_remove(disabled_subtask_categories, :subtaskCategoryId) WHERE company_id = :companyId AND service_id = :serviceId';
    const replacements = {
      companyId,
      subtaskCategoryId,
      serviceId,
    };
    await sequelizeClient.query(sql, { replacements });
    return context;
  };
};
