const axios = require('axios');
const Big = require('big.js');

const Big2 = Big();
Big2.DP = 2;

module.exports = () => {
  return async (context) => {
    const payment = context.result;
    const { shopId, shopSecret } = context.app.get('paymentSystem');
    const tariffCost = await context.app.service('tariff-costs').get(payment.tariffCostId);
    const totalCost = Big2(tariffCost.cost).times(payment.monthsNumber).toFixed(2);
    const response = await axios.post('https://payment.yandex.net/api/v3/payments', {
      amount: {
        value: totalCost,
        currency: 'RUB',
      },
      description: `Оплата подписки на ${payment.monthsNumber} мес.`,
      confirmation: {
        type: 'redirect',
        return_url: context.app.get('host'),
      },
      capture: true,
      metadata: {
        paymentId: payment.id,
        companyId: payment.companyId,
        userId: payment.userId,
        monthsNumber: payment.monthsNumber,
      },
    }, {
      auth: {
        username: shopId,
        password: shopSecret,
      },
      headers: {
        'Idempotence-Key': payment.id,
      },
    });
    const updatedPayment = await context.app.service('payments').patch(payment.id, {
      externalId: response.data.id,
      url: response.data.confirmation.confirmation_url,
    });
    context.result = updatedPayment;
    return context;
  };
};
