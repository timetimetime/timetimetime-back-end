const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    if (context.params.provider) {
      const { app } = context;
      const { query, payload } = context.params;
      const uri = query && query.uri;
      const userId = payload && payload.userId;
      if (!uri || !userId) {
        throw new errors.Forbidden();
      }
      try {
        const companies = await app.service('companies').find({
          query: { uri },
          sequelize: {
            include: [
              { model: context.app.get('sequelizeClient').models.roles },
            ],
            raw: false,
          },
        });
        const company = companies.data[0];
        if (!company) {
          throw new errors.Forbidden();
        }
        const availableTo = company.dataValues.roles.map(el => el.dataValues.userId);
        if (!availableTo.includes(userId)) {
          throw new errors.Forbidden();
        }
      } catch (err) {
        throw err;
      }
    }
    return context;
  };
};
