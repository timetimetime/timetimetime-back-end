const mailer = require('../utils/mailer');

module.exports = () => {
  return async (context) => {
    const emailChange = context.result;
    const host = context.app.get('host');
    const url = `${host}/email-confirmation?code=${emailChange.confirmationCode}`;
    mailer.send({
      To: emailChange.email,
      Subject: 'E-mail confirmation',
      HtmlBody: `<p>Please flow the <a href="${url}">link</a> to verify this e-mail address.</p>`,
    }, (error, result) => {
      if (error) {
        console.error("Unable to send via postmark: " + error.message);
      }
    });
    return context;
  };
};
