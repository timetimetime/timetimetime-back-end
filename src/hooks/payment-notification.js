const mailer = require('../utils/mailer');

module.exports = () => {
  return async (context) => {
    const payment = context.result;
    const user = await context.app.service('users').get(payment.userId);
    mailer.send({
      To: user.email,
      Subject: 'Платеж успешно обработан',
      HtmlBody: '<p></p>',
    }, (error) => {
      if (error) {
        console.error("Unable to send via postmark: " + error.message);
      }
    });
    return context;
  };
};
