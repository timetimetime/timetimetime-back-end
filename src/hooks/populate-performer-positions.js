module.exports = () => {
  return async (context) => {
    const sequelizeClient = context.app.get('sequelizeClient');
    context.params.sequelize.include = [
      {
        model: sequelizeClient.models.subtask_categories,
        as: 'subtaskCategories',
        attributes: ['id'],
      },
    ];
    return context;
  };
};
