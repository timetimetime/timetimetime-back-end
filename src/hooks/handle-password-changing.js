const bcrypt = require('bcryptjs');
const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    if (context.params.provider && context.data.password) {
      const { password, newPassword } = context.data;
      const userId = context.id;
      const user = await context.app.service('users').get(userId);
      const isPasswordCorrect = await bcrypt.compare(password, user.password);
      if (isPasswordCorrect && newPassword) {
        context.data.password = context.data.newPassword;
        delete context.data.newPassword;
      } else {
        const msg = 'Wrong current password or new password is not provided';
        throw new errors.Forbidden(msg);
      }
    }
    return context;
  };
};
