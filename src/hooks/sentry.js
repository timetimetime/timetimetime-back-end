const Raven = require('raven');

module.exports = () => {
  return async (context) => {
    if (process.env.NODE_ENV === 'production') {
      Raven.captureException(context.error, {
        extra: {
          path: context.path,
          method: context.method,
        },
      });
    }
    return context;
  };
};
