module.exports = () => {
  return async (context) => {
    const performer = context.result;
    const { subtaskCategories, companyId } = context.data;
    if (subtaskCategories) {
      await performer.setSubtaskCategories(subtaskCategories, { through: { companyId } });
      context.result.dataValues.subtaskCategories = subtaskCategories;
    }
    return context;
  };
};
