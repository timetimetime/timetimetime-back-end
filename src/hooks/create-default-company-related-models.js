module.exports = () => {
  return async (context) => {
    if (context.params.payload && context.params.payload.userId) {
      await context.app.service('roles').create({
        companyId: context.result.id,
        userId: context.params.payload.userId,
        isOwner: true,
      });
      await context.app.service('project-defaults').create({
        companyId: context.result.id,
      });
    }
    return context;
  };
};
