module.exports = () => {
  return async (context) => {
    const { companyId } = context.data;
    const defaults = await context.app.service('project-defaults').find({
      query: { companyId },
    });
    if (defaults.data.length > 0) {
      const values = defaults.data[0];
      context.data.salesmanCommission = values.salesmanCommission;
      context.data.salesmanCommissionBase = values.salesmanCommissionBase;
      context.data.taxes = values.taxes;
      context.data.insurance = values.insurance;
      context.data.income = values.income;
    }
    return context;
  };
};
