module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return context => {
    const sequelizeClient = context.app.get('sequelizeClient');
    context.params.sequelize = {
      include: [{
        model: sequelizeClient.models.subtask_categories,
        as: 'predecessors',
      }],
    };
    return context;
  };
};
