const errors = require('@feathersjs/errors');

module.exports = () => {
  return async (context) => {
    const { email } = context.data;
    const users = await context.app.service('users').find({
      query: { email },
    });
    if (users.data.length === 0) {
      throw new errors.BadRequest('USER_DOES_NOT_EXIST');
    }
    const user = users.data[0];
    if (user.isEmailConfirmed === false) {
      throw new errors.BadRequest('USER_EMAIL_IS_NOT_CONFIRMED');
    }
    context.data.userId = user.id;
    // delete context.data.email;
    return context;
  };
};
