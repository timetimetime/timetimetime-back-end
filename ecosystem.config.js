module.exports = {
  apps: [
    {
      name: 'Time API',
      script: './src/index.js',
      // instances: 0,
      // exec_mode: 'cluster',
    },
  ],
  deploy: {
    prod: {
      user: 'time',
      host: [
        {
          host: '51.15.94.103',
          port: '48488',
        },
      ],
      ref: 'origin/master',
      repo: 'git@gitlab.com:timetimetime/timetimetime-back-end.git',
      path: '/home/time/time-back-end',
      'post-deploy': 'yarn && node node_modules/db-migrate/bin/db-migrate up -e prod && /usr/local/lib/npm/bin/pm2 startOrRestart ecosystem.config.js && /usr/local/lib/npm/bin/pm2 save',
    },
  },
};
