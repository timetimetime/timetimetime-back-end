const assert = require('assert');
const rp = require('request-promise');
const url = require('url');
const app = require('../src/app');

const port = app.get('port') || 3030;
const getUrl = pathname => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: 'http',
  port,
  pathname,
});

before(async function beforeHook() {
  this.timeout(0);
  this.server = app.listen(port);
  await new Promise((resolve) => {
    this.server.once('listening', () => resolve());
  });
});
after(function afterHook(done) {
  this.server.close(done);
});

describe('Feathers.js', () => {
  it('starts and responds to http requests', () => rp(getUrl()).then(body => assert.ok(body)));
});
