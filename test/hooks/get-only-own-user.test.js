// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const getOnlyOwnUser = require('../../src/hooks/get-only-own-user');
//
// describe('\'get-only-own-user\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: getOnlyOwnUser()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
