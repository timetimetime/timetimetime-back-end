const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const sendPasswordRecoveryConfirmationLetter = require('../../src/hooks/send-password-recovery-confirmation-letter');

describe('\'send-password-recovery-confirmation-letter\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      after: sendPasswordRecoveryConfirmationLetter()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
