// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const populateRoleCompany = require('../../src/hooks/populate-role-company');
//
// describe('\'populate-role-company\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: populateRoleCompany()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
