// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const populateSubtaskCategoryPredecessors = require('../../src/hooks/populate-subtask-category-predecessors');
//
// describe('\'populateSubtaskCategoryPredecessors\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       after: populateSubtaskCategoryPredecessors()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
