const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const checkModelNumber = require('../../src/hooks/check-model-number');

describe('\'checkModelNumber\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      before: checkModelNumber()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
