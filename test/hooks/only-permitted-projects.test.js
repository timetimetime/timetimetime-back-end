// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const onlyPermittedProjects = require('../../src/hooks/only-permitted-projects');
//
// describe('\'onlyPermittedProjects\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: onlyPermittedProjects()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
