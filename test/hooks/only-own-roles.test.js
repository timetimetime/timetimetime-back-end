// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const onlyOwnRoles = require('../../src/hooks/only-own-roles');
//
// describe('\'onlyOwnRoles\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: onlyOwnRoles()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
