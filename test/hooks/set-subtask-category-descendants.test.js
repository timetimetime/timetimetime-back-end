// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const setSubtaskCategoryDescendants = require('../../src/hooks/set-subtask-category-descendants');
//
// describe('\'setSubtaskCategoryDescendants\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       after: setSubtaskCategoryDescendants()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
