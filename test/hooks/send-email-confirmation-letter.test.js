const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const sendEmailConfirmationLetter = require('../../src/hooks/send-email-confirmation-letter');

describe('\'sendEmailConfirmationLetter\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      after: sendEmailConfirmationLetter()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
