// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const onlyOwnCompanies = require('../../src/hooks/only-own-companies');
//
// describe('\'onlyOwnCompanies\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: onlyOwnCompanies()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
