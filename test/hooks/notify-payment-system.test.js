const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const notifyPaymentSystem = require('../../src/hooks/notify-payment-system');

describe('\'notify-payment-system\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      after: notifyPaymentSystem()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
