const assert = require('assert');
const feathers = require('@feathersjs/feathers');
const handleNewUserEmailConfirmation = require('../../src/hooks/handle-new-user-email-confirmation');

describe('\'handleNewUserEmailConfirmation\' hook', () => {
  let app;

  beforeEach(() => {
    app = feathers();

    app.use('/dummy', {
      async get(id) {
        return { id };
      }
    });

    app.service('dummy').hooks({
      after: handleNewUserEmailConfirmation()
    });
  });

  it('runs the hook', async () => {
    const result = await app.service('dummy').get('test');
    
    assert.deepEqual(result, { id: 'test' });
  });
});
