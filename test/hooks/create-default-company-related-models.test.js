// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const createDefaultCompanyRelatedModels = require('../../src/hooks/create-default-company-related-models');
//
// describe('\'createDefaultCompanyRelatedModels\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       after: createDefaultCompanyRelatedModels()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
