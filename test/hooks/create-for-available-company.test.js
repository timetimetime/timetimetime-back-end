// const assert = require('assert');
// const feathers = require('@feathersjs/feathers');
// const createForAvailableCompany = require('../../src/hooks/create-for-available-company');
//
// describe('\'createForAvailableCompany\' hook', () => {
//   let app;
//
//   beforeEach(() => {
//     app = feathers();
//
//     app.use('/dummy', {
//       async get(id) {
//         return { id };
//       }
//     });
//
//     app.service('dummy').hooks({
//       before: createForAvailableCompany()
//     });
//   });
//
//   it('runs the hook', async () => {
//     const result = await app.service('dummy').get('test');
//
//     assert.deepEqual(result, { id: 'test' });
//   });
// });
