const assert = require('assert');
const app = require('../../src/app');

describe('\'TariffCosts\' service', () => {
  it('registered the service', () => {
    const service = app.service('tariff-costs');

    assert.ok(service, 'Registered the service');
  });
});
