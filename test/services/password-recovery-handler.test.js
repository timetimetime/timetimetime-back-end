const assert = require('assert');
const app = require('../../src/app');

describe('\'passwordRecoveryHandler\' service', () => {
  it('registered the service', () => {
    const service = app.service('password-recovery-handler');

    assert.ok(service, 'Registered the service');
  });
});
