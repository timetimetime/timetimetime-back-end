const assert = require('assert');
const app = require('../../src/app');

describe('\'EmailChanges\' service', () => {
  it('registered the service', () => {
    const service = app.service('email-changes');

    assert.ok(service, 'Registered the service');
  });
});
