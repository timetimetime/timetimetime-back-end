const assert = require('assert');
const app = require('../../src/app');

describe('\'SubtaskCategoriesOrder\' service', () => {
  it('registered the service', () => {
    const service = app.service('subtask-categories-order');

    assert.ok(service, 'Registered the service');
  });
});
