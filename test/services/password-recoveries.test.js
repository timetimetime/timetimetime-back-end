const assert = require('assert');
const app = require('../../src/app');

describe('\'PasswordRecoveries\' service', () => {
  it('registered the service', () => {
    const service = app.service('password-recoveries');

    assert.ok(service, 'Registered the service');
  });
});
