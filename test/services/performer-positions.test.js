const assert = require('assert');
const app = require('../../src/app');

describe('\'PerformerPositions\' service', () => {
  it('registered the service', () => {
    const service = app.service('performer-positions');

    assert.ok(service, 'Registered the service');
  });
});
