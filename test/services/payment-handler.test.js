const assert = require('assert');
const app = require('../../src/app');

describe('\'PaymentHandler\' service', () => {
  it('registered the service', () => {
    const service = app.service('payment-handler');

    assert.ok(service, 'Registered the service');
  });
});
