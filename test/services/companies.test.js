const assert = require('assert');
const rp = require('request-promise');
const url = require('url');
const app = require('../../src/app');

const port = app.get('port') || 3030;
const getUrl = pathname => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: 'http',
  port,
  pathname,
});

const password = '1234567';
const global = {};

describe('Companies service', () => {
  before(async function beforeHook() {
    this.timeout(0);
    global.user = await app.service('users').create({
      email: `own-${Date.now()}@own.own`,
      password,
      firstName: 'own',
      lastName: 'own',
    });
    global.company = await app.service('companies').create({
      name: `company-${Date.now()}`,
      uri: `company-${Date.now()}`,
    });
    global.role = await app.service('roles').create({
      companyId: global.company.id,
      userId: global.user.id,
    });
    const authReq = await rp({
      method: 'POST',
      uri: `${getUrl()}/authentication`,
      body: {
        email: global.user.email,
        password,
        strategy: 'local',
      },
      json: true,
    });
    global.accessToken = authReq.accessToken;
    return true;
  });
  // after(function afterHook(done) {
  //   this.server.close(done);
  // });
  describe('#find', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'GET',
      uri: `${getUrl()}/companies`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when no "?uri"', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/companies`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => {
          throw new Error();
        })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when requests available companies', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/companies?uri=${global.company.uri}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
// });
  // describe('#get', () => {
  //   it('returns 401 when the client is not authenticated', () => rp({
  //     method: 'GET',
  //     uri: `${getUrl()}/users/1`,
  //     json: true,
  //   }).catch(err => assert.ok(err.statusCode === 401)));
  //   it('returns 403 when other user requested (not own)', async () => rp({
  //     method: 'GET',
  //     uri: `${getUrl()}/users/${global.anotherUser.id}`,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).catch(err => assert.ok(err.statusCode === 403)));
  //   it('returns 200 when own user requested', () => rp({
  //     uri: `${getUrl()}/users/${global.user.id}`,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).then(body => assert.ok(body)));
  // });
  // describe('#create', () => {
  //   it('creates and returns record', () => rp({
  //     method: 'POST',
  //     uri: `${getUrl()}/users`,
  //     body: {
  //       email: `new-${Date.now()}@new.new`,
  //       password,
  //       firstName: 'new',
  //       lastName: 'new',
  //     },
  //     json: true,
  //   }).then(body => assert.ok(body)));
  // });
  // describe('#update', () => {
  //   it('returns 405', () => rp({
  //     method: 'PUT',
  //     uri: `${getUrl()}/users/1`,
  //     json: true,
  //   }).catch(err => assert.ok(err.statusCode === 405)));
  // });
  // describe('#patch', () => {
  //   it('returns 403 when trying to patch another user', () => rp({
  //     method: 'PATCH',
  //     uri: `${getUrl()}/users/${global.anotherUser.id}`,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).catch(err => assert.ok(err.statusCode === 403)));
  //   it('returns 200 when trying to patch own user', () => rp({
  //     method: 'PATCH',
  //     uri: `${getUrl()}/users/${global.user.id}`,
  //     body: global.user,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).then(body => assert.ok(body)));
  // });
  // describe('#remove', () => {
  //   it('returns 403 when trying to delete another user', () => rp({
  //     method: 'DELETE',
  //     uri: `${getUrl()}/users/${global.anotherUser.id}`,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).catch(err => assert.ok(err.statusCode === 403)));
  //   it('returns 200 when trying to delete own user', () => rp({
  //     method: 'DELETE',
  //     uri: `${getUrl()}/users/${global.user.id}`,
  //     body: global.user,
  //     headers: {
  //       Authorization: `Bearer ${global.accessToken}`,
  //     },
  //     json: true,
  //   }).then(body => assert.ok(body)));
  // });
});
