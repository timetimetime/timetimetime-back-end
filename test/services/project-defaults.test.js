const assert = require('assert');
const app = require('../../src/app');

describe('\'project-defaults\' service', () => {
  it('registered the service', () => {
    const service = app.service('project-defaults');

    assert.ok(service, 'Registered the service');
  });
});
