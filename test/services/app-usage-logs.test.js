const assert = require('assert');
const app = require('../../src/app');

describe('\'appUsageLogs\' service', () => {
  it('registered the service', () => {
    const service = app.service('app-usage-logs');

    assert.ok(service, 'Registered the service');
  });
});
