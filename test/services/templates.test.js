const assert = require('assert');
const rp = require('request-promise');
const url = require('url');
const app = require('../../src/app');

const port = app.get('port') || 3030;
const getUrl = pathname => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: 'http',
  port,
  pathname,
});

const serviceName = 'templates';
const getDefaultAttributes = () => {
  return {
    name: 'name',
  };
};
const passwordForMockUser = '1234567';
const global = {};

describe('Templates service', () => {
  before(async function beforeHook() {
    this.timeout(0);
    global.user = await app.service('users').create({
      email: `own-${Date.now()}@own.own`,
      password: passwordForMockUser,
      firstName: 'own',
      lastName: 'own',
    });
    global.company = await app.service('companies').create({
      name: `company-${Date.now()}`,
      uri: `company-${Date.now()}`,
    });
    global.role = await app.service('roles').create({
      companyId: global.company.id,
      userId: global.user.id,
    });
    global.anotherCompany = await app.service('companies').create({
      name: `anc-${Date.now()}`,
      uri: `anc-${Date.now()}`,
    });
    global.record = await app.service(serviceName).create(Object
      .assign({}, getDefaultAttributes(), {
        companyId: global.company.id,
      }));
    global.anotherRecord = await app.service(serviceName).create(Object
      .assign({}, getDefaultAttributes(), {
        companyId: global.anotherCompany.id,
      }));
    const authReq = await rp({
      method: 'POST',
      uri: `${getUrl()}/authentication`,
      body: {
        email: global.user.email,
        password: passwordForMockUser,
        strategy: 'local',
      },
      json: true,
    });
    global.accessToken = authReq.accessToken;
    return true;
  });
  after(async () => {
    await app.service('users').remove(global.user.id);
    await app.service('companies').remove(global.company.id);
    await app.service('companies').remove(global.anotherCompany.id);
  });
  describe('#find', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'GET',
      uri: `${getUrl()}/${serviceName}`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when no "?companyId"', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/${serviceName}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "403 forbidden" when requested company is not available', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/${serviceName}?${global.anotherCompany.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when requests for available companies', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/${serviceName}?companyId=${global.company.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
  describe('#get', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'GET',
      uri: `${getUrl()}/${serviceName}/1`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when get for not available companies', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/${serviceName}/${global.anotherRecord.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when get for available companies', () =>
      rp({
        method: 'GET',
        uri: `${getUrl()}/${serviceName}/${global.record.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
  describe('#create', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'POST',
      uri: `${getUrl()}/${serviceName}`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when creates for not available companies', () =>
      rp({
        method: 'POST',
        uri: `${getUrl()}/${serviceName}`,
        body: {
          companyId: global.anotherCompany.id,
        },
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when creates for available companies', () =>
      rp({
        method: 'POST',
        uri: `${getUrl()}/${serviceName}`,
        body: Object.assign({}, getDefaultAttributes(), {
          companyId: global.company.id,
        }),
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
  describe('#patch', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'PATCH',
      uri: `${getUrl()}/${serviceName}/1`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when patch for not available companies', () =>
      rp({
        method: 'PATCH',
        uri: `${getUrl()}/${serviceName}/${global.anotherRecord.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when patch for available companies', () =>
      rp({
        method: 'PATCH',
        uri: `${getUrl()}/${serviceName}/${global.record.id}`,
        body: {},
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
  describe('#remove', () => {
    it('returns "401 unauthorized" when no accessToken provided', () => rp({
      method: 'DELETE',
      uri: `${getUrl()}/${serviceName}/1`,
      json: true,
    })
      .then(() => { throw new Error(); })
      .catch(err => assert.strictEqual(err.statusCode, 401)));
    it('returns "403 forbidden" when remove for not available companies', () =>
      rp({
        method: 'DELETE',
        uri: `${getUrl()}/${serviceName}/${global.anotherRecord.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(() => { throw new Error(); })
        .catch(err => assert.strictEqual(err.statusCode, 403)));
    it('returns "200 ok" when remove for available companies', () =>
      rp({
        method: 'DELETE',
        uri: `${getUrl()}/${serviceName}/${global.record.id}`,
        headers: {
          Authorization: `Bearer ${global.accessToken}`,
        },
        json: true,
      })
        .then(body => assert.ok(body)));
  });
});
