const assert = require('assert');
const app = require('../../src/app');

describe('\'Tariffs\' service', () => {
  it('registered the service', () => {
    const service = app.service('tariffs');

    assert.ok(service, 'Registered the service');
  });
});
