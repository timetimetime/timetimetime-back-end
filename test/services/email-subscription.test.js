const assert = require('assert');
const app = require('../../src/app');

describe('\'EmailSubscription\' service', () => {
  it('registered the service', () => {
    const service = app.service('email-subscription');

    assert.ok(service, 'Registered the service');
  });
});
