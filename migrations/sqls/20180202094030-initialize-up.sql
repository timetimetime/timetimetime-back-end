-- Table: users

CREATE TABLE users
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  email character varying(255) UNIQUE NOT NULL,
  password character(60) NOT NULL,
  first_name character varying(255),
  last_name character varying(255),
  is_email_confirmed boolean NOT NULL DEFAULT false,
  receive_email_notifications boolean NOT NULL DEFAULT true,
  unsubscription_code uuid NOT NULL DEFAULT gen_random_uuid(),
  show_tutorial boolean NOT NULL DEFAULT true,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: email_changes

CREATE TABLE email_changes
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  email character varying(255) NOT NULL,
  confirmation_code uuid NOT NULL DEFAULT gen_random_uuid(),
  is_confirmed boolean NOT NULL DEFAULT false,
  user_id uuid REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: password_recoveries

CREATE TABLE password_recoveries
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  user_id uuid REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  confirmation_code uuid NOT NULL DEFAULT gen_random_uuid(),
  is_confirmed boolean NOT NULL DEFAULT false,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: companies

CREATE TABLE companies
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  uri character varying(255) UNIQUE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: roles

CREATE TABLE roles
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  user_id uuid REFERENCES users ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: expenses

CREATE TABLE expenses
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  cost integer CHECK (cost >= 0) NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: services

CREATE TABLE services
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: subtask_categories

CREATE TABLE subtask_categories
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  service_id uuid REFERENCES services ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: subtask_categories_order

CREATE TABLE subtask_categories_order
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  subtask_category_id uuid REFERENCES subtask_categories ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  predecessor_id uuid REFERENCES subtask_categories ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  UNIQUE (subtask_category_id, predecessor_id)
);

-- Table: performers

CREATE TABLE performers
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  salary integer CHECK (salary >= 0) NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: performer_positions

CREATE TABLE performer_positions
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  subtask_category_id uuid REFERENCES subtask_categories ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  performer_id uuid REFERENCES performers ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  UNIQUE (subtask_category_id, performer_id)
);

-- Table: projects

CREATE TYPE enum_project_types AS ENUM ('agile', 'waterfall');

CREATE TYPE enum_projects_salesman_commission_base AS ENUM ('total', 'income');

CREATE TABLE projects
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  salesman_commission smallint CHECK (salesman_commission >= 0 AND salesman_commission <= 100) NOT NULL DEFAULT 0,
  salesman_commission_base enum_projects_salesman_commission_base NOT NULL DEFAULT 'total',
  taxes smallint CHECK (taxes >= 0 AND taxes <= 100) NOT NULL DEFAULT 0,
  insurance smallint CHECK (insurance >= 0 AND insurance <= 100) NOT NULL DEFAULT 0,
  income smallint CHECK (income >= 0 AND income <= 100) NOT NULL DEFAULT 0,
  type enum_project_types NOT NULL,
  uri character varying(255) NOT NULL,
  disabled_subtask_categories uuid[] NOT NULL DEFAULT ARRAY[]::uuid[],
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  service_id uuid REFERENCES services ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  UNIQUE (company_id, uri)
);

-- Table: project_defaults

CREATE TABLE project_defaults
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  salesman_commission smallint CHECK (salesman_commission >= 0 AND salesman_commission <= 100) NOT NULL DEFAULT 0,
  salesman_commission_base enum_projects_salesman_commission_base NOT NULL DEFAULT 'total',
  taxes smallint CHECK (taxes >= 0 AND taxes <= 100) NOT NULL DEFAULT 0,
  insurance smallint CHECK (insurance >= 0 AND insurance <= 100) NOT NULL DEFAULT 0,
  income smallint CHECK (income >= 0 AND income <= 100) NOT NULL DEFAULT 0,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE UNIQUE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: tasks

CREATE TABLE tasks
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  project_id uuid REFERENCES projects ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: subtasks

CREATE TABLE subtasks
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  duration smallint CHECK (duration >= 0) NOT NULL DEFAULT 0,
  task_id uuid REFERENCES tasks ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  subtask_category_id uuid REFERENCES subtask_categories ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  performer_id uuid REFERENCES performers ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: user_actions

CREATE TYPE enum_user_actions_action AS ENUM
    ('open_app', 'go_to', 'close_app', 'create', 'read', 'update', 'delete', 'get_estimation');

CREATE TYPE enum_user_actions_client AS ENUM
    ('web', 'mobile');

CREATE TYPE enum_user_actions_type AS ENUM
    ('navigation', 'crud', 'various');

CREATE TABLE user_actions
(
  user_id uuid,
  client enum_user_actions_client NOT NULL,
  action enum_user_actions_action NOT NULL,
  type enum_user_actions_type NOT NULL,
  payload jsonb,
  "time" timestamp with time zone NOT NULL DEFAULT now()
);

SELECT create_hypertable('user_actions', 'time');

-- Table: registrations

CREATE TABLE registrations
(
  ip inet NOT NULL,
  "time" timestamp with time zone NOT NULL DEFAULT now()
);

SELECT create_hypertable('registrations', 'time');

-- Table: currencies

CREATE TABLE currencies
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  code character varying(3) NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: tariffs

CREATE TABLE tariffs
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  name character varying(255) NOT NULL,
  number_of_projects integer CHECK (number_of_projects >= 0) NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Table: tariff_costs

CREATE TABLE tariff_costs
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  cost numeric(12, 2) NOT NULL,
  tariff_id uuid REFERENCES tariffs ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  currency_id uuid REFERENCES currencies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now(),
  UNIQUE (tariff_id, currency_id)
);

-- Table: payments

CREATE TABLE payments
(
  id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
  external_id uuid,
  months_number integer NOT NULL,
  url character varying(255),
  is_paid boolean NOT NULL DEFAULT false,
  paid_till timestamp with time zone,
  reception_time timestamp with time zone,
  company_id uuid REFERENCES companies ON UPDATE CASCADE ON DELETE CASCADE NOT NULL,
  user_id uuid REFERENCES users ON UPDATE CASCADE ON DELETE RESTRICT NOT NULL,
  tariff_cost_id uuid REFERENCES tariff_costs ON UPDATE CASCADE ON DELETE RESTRICT NOT NULL,
  created_at timestamp with time zone NOT NULL DEFAULT now(),
  updated_at timestamp with time zone NOT NULL DEFAULT now()
);

-- Common data

INSERT INTO currencies(id, code)
	VALUES ('51b5fb72-4081-4c04-a98a-75669d7570c2', 'RUB');

INSERT INTO tariffs(id, name, number_of_projects)
	VALUES ('60dd23d3-4e3f-4816-98c2-07c43f409468', 'Lite', 8);

INSERT INTO tariff_costs(cost, tariff_id, currency_id)
	VALUES (450, '60dd23d3-4e3f-4816-98c2-07c43f409468', '51b5fb72-4081-4c04-a98a-75669d7570c2');

INSERT INTO tariffs(id, name, number_of_projects)
	VALUES ('ba9c0ec0-166d-4b4b-a4b7-bc4c31f44b6b', 'Standard', 32);

INSERT INTO tariff_costs(cost, tariff_id, currency_id)
	VALUES (900, 'ba9c0ec0-166d-4b4b-a4b7-bc4c31f44b6b', '51b5fb72-4081-4c04-a98a-75669d7570c2');

INSERT INTO tariffs(id, name, number_of_projects)
	VALUES ('f45001d5-bd23-4db9-91de-a8c0979cc34f', 'Pro', 128);

INSERT INTO tariff_costs(cost, tariff_id, currency_id)
	VALUES (1800, 'f45001d5-bd23-4db9-91de-a8c0979cc34f', '51b5fb72-4081-4c04-a98a-75669d7570c2');

INSERT INTO tariffs(id, name, number_of_projects)
	VALUES ('ae4159d3-ddfa-4078-a8ec-0a4c38287eb3', 'Maximum', 512);

INSERT INTO tariff_costs(cost, tariff_id, currency_id)
	VALUES (3600, 'ae4159d3-ddfa-4078-a8ec-0a4c38287eb3', '51b5fb72-4081-4c04-a98a-75669d7570c2');
