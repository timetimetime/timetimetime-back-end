const {
  POSTGRESQL_HOST,
  POSTGRESQL_DATABASE,
  POSTGRESQL_USER,
  POSTGRESQL_PASSWORD,
  JWT_SECRET,
  SHOP_ID,
  SHOP_SECRET,
} = process.env;

module.exports = {
  host: 'http://192.168.33.10',
  port: 3030,
  public: '../public/',
  paginate: {
    default: 10,
    max: 50,
  },
  postgres: `postgres://${POSTGRESQL_USER}:${POSTGRESQL_PASSWORD}@${POSTGRESQL_HOST}:6432/${POSTGRESQL_DATABASE}`,
  authentication: {
    secret: JWT_SECRET,
    strategies: [
      'jwt',
      'local',
    ],
    path: '/authentication',
    service: 'users',
    jwt: {
      header: {
        typ: 'access',
      },
      issuer: 'feathers',
      algorithm: 'HS256',
      expiresIn: '1d',
    },
    local: {
      entity: 'user',
      usernameField: 'email',
      passwordField: 'password',
    },
  },
  paymentSystem: {
    shopId: SHOP_ID,
    shopSecret: SHOP_SECRET,
  },
};
